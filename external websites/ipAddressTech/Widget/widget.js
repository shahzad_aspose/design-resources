$(document).ready(function() {

    var $element = $('input[type="range"]');
    var $handle;

    $element.rangeslider({
        polyfill: false,
        onInit: function(rule, targetId, value) {
            $handle = $('.rangeslider__handle', this.$range);
            updateHandle($handle[0], this.value);
        }
    }).on('input', function() {
        updateHandle($handle[0], this.value);
    });

    function updateHandle(el, val) {
        el.textContent = val;
    }

    $('input[type="range"]').rangeslider();
    
});
        function setStyleOn(rule, targetId, value) {
            x = document.getElementById(targetId);
            x.style[rule] = value;
        }
// show-widget onChange 
function showIMG(){
    document.getElementById('widgetTXT').style.display = 'none';
      document.getElementById('widgetIMG').style.display ='block';
}
function showTXT(){
    document.getElementById('widgetIMG').style.display ='none';    
  document.getElementById('widgetTXT').style.display = 'block';
}
// Background-Color onChange 
    $('#bgcolor').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = '#' +this.value;
    $(".bgcolor").css("background-color", valueSelected);
    });        
// Border-Color onChange 
$('#bordercolor').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = '#' +this.value;
    $(".bordercolor").css("border-color", valueSelected);
    });        
// Background-Color onChange 
$('#textcolor').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = '#' +this.value;
    $(".textcolor").css("color", valueSelected);
    });   