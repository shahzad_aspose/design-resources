//Sidebar Responsive
$('.sidebarbtn a').click(function(){
    $('.containerizebody .blueside').toggleClass('widzero');
     $('.sidebarbtn').toggleClass('btnclicked');
    });
//Show hide password input field    
$('.hidepass').hide();
$('.showpass').click(function() {
    $('#newpass').attr('type','text');
    $('.hidepass').show();
    $('.showpass').hide();

});
$('.hidepass').click(function() {
    $('#newpass').attr('type','password');
    $('.hidepass').hide();
    $('.showpass').show();
});
// Timzone
;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'span' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
//checkbox UI
$('.ch-text span').on('click', function(){
	var checkbox = $('.ch-text input#checkbox-1-2');
	checkbox.prop('checked', !checkbox.prop('checked'));
 });
//input hover 
$(function() {
    $('.formsection label').click(function() {
        if ($('.formsection label').hasClass("linehover")) {
			$('.formsection label').removeClass('lineanimate');
			$(this).addClass('lineanimate');
        }
        else if ($('.formsection label').hasClass("lineanimate")) {
        }
    });
});